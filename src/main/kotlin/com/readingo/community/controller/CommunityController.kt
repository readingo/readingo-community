package com.readingo.community.controller

import com.readingo.community.model.Community
import com.readingo.community.model.SuccessObject
import com.readingo.community.model.dto.BookDTO
import com.readingo.community.model.dto.CommunityCreateDTO
import com.readingo.community.model.dto.MemberDTO
import com.readingo.community.model.makeConflict
import com.readingo.community.model.makeNotFound
import com.readingo.community.repository.CommunityRepository
import com.readingo.community.repository.UserRepository
import com.readingo.community.utils.getUserFromJWT
import org.springframework.data.repository.findByIdOrNull
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@Validated
class CommunityController(
		private val communityRepository: CommunityRepository,
		private val userRepository: UserRepository
) : ErrorHandlingController {
	@GetMapping("/{id}")
	fun getCommunityById(@PathVariable id: String) = communityRepository.findById(id)

	@PostMapping("/")
	fun createCommunity(@Valid @RequestBody community: CommunityCreateDTO): Community = communityRepository.save(community.toCommunity())

	@PostMapping("/{id}/member")
	fun addMemberToCommunity(@PathVariable id: String, @Valid @RequestBody dto: MemberDTO): Any {
		val user = userRepository.findByIdOrNull(dto.member) ?: return makeNotFound("User not found")
		val community = communityRepository.findByIdOrNull(id) ?: return makeNotFound("Community not found")
		return try {
			communityRepository.save(community.addMember(user))
			SuccessObject()
		} catch (e: Exception) {
			makeConflict(e.message)
		}
	}

	@DeleteMapping("/{id}/member")
	fun kickMemberFromCommunity(@PathVariable id: String, @Valid @RequestBody dto: MemberDTO): Any {
		val user = userRepository.findByIdOrNull(dto.member) ?: return makeNotFound("User not found")
		val community = communityRepository.findByIdOrNull(id) ?: return makeNotFound("Community not found")
		return try {
			user.community = null
			userRepository.save(user)
			communityRepository.save(community.kickMember(user))
			SuccessObject()
		} catch (e: Exception) {
			makeConflict(e.message)
		}
	}

	@PostMapping("/book")
	fun addBookToCommunity(@Valid @RequestBody dto: BookDTO): Any {
		val user = userRepository.findById(getUserFromJWT(dto.jwt)).orElse(null)
				?: return makeNotFound("User not found")
		val community = user.community ?: return makeNotFound("Community not found")
		return try {
			community.books.add(dto.book)
			communityRepository.save(community)
		} catch (e: Exception) {
			makeConflict(e.message)
		}
	}

	@DeleteMapping("/book")
	fun removeBookFromCommunity(@Valid @RequestBody dto: BookDTO): Any {
		val user = userRepository.findById(getUserFromJWT(dto.jwt)).orElse(null)
				?: return makeNotFound("User not found")
		val community = user.community ?: return makeNotFound("Community not found")
		return try {
			community.books.remove(dto.book)
			communityRepository.save(community)
		} catch (e: Exception) {
			makeConflict(e.message)
		}
	}
}

package com.readingo.community.controller

import com.fasterxml.jackson.databind.exc.ValueInstantiationException
import com.readingo.community.model.ErrorObject
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.http.converter.HttpMessageNotReadableException
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus

interface ErrorHandlingController {
	@ExceptionHandler(HttpMessageNotReadableException::class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	fun onHttpMessageNotReadableException(exception: HttpMessageNotReadableException): ResponseEntity<ErrorObject> {
		return ResponseEntity(ErrorObject(exception.message), HttpStatus.INTERNAL_SERVER_ERROR)
	}

	@ExceptionHandler(ValueInstantiationException::class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	fun onValueInstantiationException(exception: ValueInstantiationException): ResponseEntity<ErrorObject> {
		return ResponseEntity(ErrorObject("Request body is malformed"), HttpStatus.INTERNAL_SERVER_ERROR)
	}

	@ExceptionHandler(MethodArgumentNotValidException::class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	fun onMethodArgumentNotValidException(exception: MethodArgumentNotValidException): ResponseEntity<Any> {
		val validationErrors = exception.bindingResult.allErrors.joinToString { it.defaultMessage ?: "" }
		return ResponseEntity(ErrorObject(validationErrors), HttpStatus.INTERNAL_SERVER_ERROR)
	}
}
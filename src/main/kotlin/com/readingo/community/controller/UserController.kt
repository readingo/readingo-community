package com.readingo.community.controller

import com.readingo.community.model.SuccessObject
import com.readingo.community.model.User
import com.readingo.community.model.dto.JWTTransportDTO
import com.readingo.community.model.dto.LoginUserDTO
import com.readingo.community.model.dto.RegisterUserDTO
import com.readingo.community.model.makeConflict
import com.readingo.community.model.makeNotFound
import com.readingo.community.repository.UserRepository
import com.readingo.community.utils.createJWT
import com.readingo.community.utils.hashPassword
import com.readingo.community.utils.verifyPassword
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@Validated
class UserController(
		private val userRepository: UserRepository
) : ErrorHandlingController {
	@GetMapping("/user/{id}")
	fun getUser(@PathVariable("id") id: String) = userRepository.findById(id).orElse(null)
			?: throw Exception("No such user")

	@PostMapping("/user/login")
	fun loginUser(@Valid @RequestBody(required = true) user: LoginUserDTO): Any {
		val foundUser = userRepository.findByEmail(user.email) ?: return makeNotFound("Invalid email or password")

		return if (verifyPassword(user.password, foundUser.passwordHash)) {
			JWTTransportDTO(createJWT(foundUser.id!!), foundUser)
		} else makeNotFound("Invalid email or password")

	}

	@PostMapping("/user/register")
	fun registerUser(@Valid @RequestBody(required = true) user: RegisterUserDTO): Any {
		val foundUser = userRepository.findByEmail(user.email)

		if (foundUser != null) return makeConflict("User already exists")

		val hashedPassword = hashPassword(user.password)
		val userToSave = User(null, user.username, user.email, hashedPassword, null)
		userRepository.save(userToSave)
		return SuccessObject()

	}
}
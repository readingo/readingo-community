package com.readingo.community.controller

import com.readingo.community.model.ErrorObject
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler


@ControllerAdvice
class CustomGlobalExceptionHandler : ResponseEntityExceptionHandler() {
	@ExceptionHandler(Exception::class)
	fun springHandleNotFound(exception: Exception): ResponseEntity<Any> {
		return ResponseEntity(ErrorObject(exception.message), HttpStatus.INTERNAL_SERVER_ERROR)
	}

	//...
}

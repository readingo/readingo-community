package com.readingo.community.controller

import com.readingo.community.model.SuccessObject
import com.readingo.community.model.User
import com.readingo.community.model.makeConflict
import com.readingo.community.model.makeNotFound
import com.readingo.community.repository.CommunityRepository
import com.readingo.community.repository.UserRepository
import com.readingo.community.utils.decodeJWT
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RestController

val noAuthRouteBlacklist = listOf("/api/community/user/login", "/api/community/user/register")

fun validateRoute(method: String, route: String, user: User, communityRepository: CommunityRepository): Boolean {
	val splitRoute = route.split('/').drop(1)
	if (route.startsWith("/api/book")) {
		val split = splitRoute.drop(2)
		return if (split.isNotEmpty()) {
			val bookId = split[0]
			val community = communityRepository.findById(user.community?.id ?: "").orElse(null)
			community?.books?.contains(bookId) ?: false
		} else true
	} else if (route.startsWith("/api/community")) {
		if (route == "/api/community" && method.equals("post", ignoreCase = true)) {
			return user.community == null
		}

		if (splitRoute.size == 3 && method.equals("get", ignoreCase = true)) {
			return user.community?.id == splitRoute[2]
		}

		if (route.startsWith("/api/community/user/")) {
			val id = splitRoute.drop(3)[0]
			return id == user.id
		}
	}
	return false
}

@RestController
class AuthController(
		private val communityRepository: CommunityRepository,
		private val userRepository: UserRepository
) {
	@GetMapping("/auth")
	fun authenticateRequest(
			@RequestHeader("Authorization", required = false) bearerToken: String?,
			@RequestHeader("X-Forwarded-Uri") requestUrl: String,
			@RequestHeader("X-Forwarded-Method") requestMethod: String
	): Any {
		if (noAuthRouteBlacklist.contains(requestUrl)) {
			return "OK"
		}

		val token = try {
			decodeJWT(bearerToken ?: "")
		} catch (e: Exception) {
			return makeConflict("Invalid or missing authorization token")
		}

		return if (token.audience.size > 0) {
			val userId = token.audience[0]
			val foundUser: User? = userRepository.findById(userId).orElseGet(null)
			when {
				foundUser == null -> makeNotFound("No user found")
				validateRoute(requestMethod, requestUrl, foundUser, communityRepository) -> SuccessObject()
				else -> makeNotFound("Unauthorized")
			}

		} else makeNotFound("No token audience")
	}
}
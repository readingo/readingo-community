package com.readingo.community.utils

import at.favre.lib.crypto.bcrypt.BCrypt

fun hashPassword(password: String) = BCrypt.withDefaults().hashToString(12, password.toCharArray())
fun verifyPassword(password: String, passwordHash: String) = BCrypt.verifyer().verify(password.toByteArray(), passwordHash.toByteArray()).verified
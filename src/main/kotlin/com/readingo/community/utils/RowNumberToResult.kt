package com.readingo.community.utils

import com.readingo.community.model.ErrorObject
import com.readingo.community.model.Result
import com.readingo.community.model.SuccessObject

enum class RowNumberError { ON_ZERO, ON_NUMBER }

fun rowNumberToResult(rowNumber: Int, errorReason: String?, error: RowNumberError = RowNumberError.ON_ZERO): Result {
	return when (error) {
		RowNumberError.ON_ZERO -> if (rowNumber == 0) ErrorObject(errorReason) else SuccessObject()
		RowNumberError.ON_NUMBER -> if (rowNumber != 0) ErrorObject(errorReason) else SuccessObject()
	}
}
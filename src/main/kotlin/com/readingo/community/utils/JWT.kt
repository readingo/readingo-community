package com.readingo.community.utils

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import com.auth0.jwt.interfaces.DecodedJWT
import java.util.*

fun createJWT(userId: String): String = JWT.create().apply {
	withIssuer("readingo")
	withIssuedAt(Date())
	withAudience(userId)
}.sign(Algorithm.HMAC256("secret"))

fun decodeJWT(jwt: String): DecodedJWT =
		JWT.require(Algorithm.HMAC256("secret")).apply {
			withIssuer("readingo")
		}.build().verify(jwt) ?: throw Exception("Invalid authorization header")

fun getUserFromJWT(jwt: String): String = decodeJWT(jwt).audience[0]
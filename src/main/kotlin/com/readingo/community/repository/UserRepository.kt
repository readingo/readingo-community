package com.readingo.community.repository

import com.readingo.community.model.User
import org.springframework.data.repository.CrudRepository

interface UserRepository : CrudRepository<User, String> {
	fun findByEmail(email: String): User?
}
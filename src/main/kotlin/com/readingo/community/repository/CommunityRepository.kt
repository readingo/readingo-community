package com.readingo.community.repository

import com.readingo.community.model.Community
import org.springframework.data.repository.CrudRepository

interface CommunityRepository : CrudRepository<Community, String> {
	fun findByName(name: String): List<Community>
}
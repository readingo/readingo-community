package com.readingo.community.model

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import org.hibernate.annotations.GenericGenerator
import javax.persistence.*
import javax.validation.constraints.Email
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

@Entity(name = "CommunityUser")
data class User(
		@Id
		@GeneratedValue(generator = "system-uuid")
		@GenericGenerator(name = "system-uuid", strategy = "uuid")
		val id: String? = null,

		@field:NotEmpty(message = "Username must be provided")
		@field:NotNull(message = "Username must not be null")
		val username: String,

		@field:NotEmpty(message = "Email must be provided")
		@field:NotNull(message = "Email must not be null")
		@field:Email(message = "Not valid email")
		val email: String,

		@field:NotEmpty(message = "Password must be provided")
		@field:NotNull(message = "Password must not be null")
		@field:JsonIgnore()
		val passwordHash: String,

		@ManyToOne(fetch = FetchType.LAZY, optional = true)
		@JsonIgnoreProperties("members")
		var community: Community?
)
package com.readingo.community.model.dto

import javax.validation.constraints.Email
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

data class RegisterUserDTO(
		@field:NotEmpty(message = "User name must be provided")
		@field:NotNull(message = "User name must not be null")
		val username: String,

		@field:NotEmpty(message = "Email must be provided")
		@field:NotNull(message = "Email must not be null")
		@field:Email(message = "Email is not valid")
		val email: String,

		@field:NotEmpty(message = "Password must be provided")
		@field:NotNull(message = "Password must not be null")
		val password: String
)
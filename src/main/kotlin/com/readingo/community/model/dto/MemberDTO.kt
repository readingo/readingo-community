package com.readingo.community.model.dto

import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

class MemberDTO(
		@field:NotEmpty(message = "Member must be provided")
		@field:NotNull(message = "Member must not be null")
		val member: String = ""
)
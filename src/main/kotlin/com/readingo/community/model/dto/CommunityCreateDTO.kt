package com.readingo.community.model.dto

import com.readingo.community.model.Community
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

data class CommunityCreateDTO(
		@field:NotEmpty(message = "Name must be provided")
		@field:NotNull(message = "Name must not be null")
		val name: String,

		@field:NotEmpty(message = "Description must be provided")
		@field:NotNull(message = "Description must not be null")
		val description: String
) {
	fun toCommunity() = Community(null, this.name, this.description, mutableListOf(), mutableListOf())
}
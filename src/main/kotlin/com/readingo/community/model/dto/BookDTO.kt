package com.readingo.community.model.dto

import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

data class BookDTO(
		@field:NotEmpty(message = "Book must be provided")
		@field:NotNull(message = "Book must not be null")
		val book: String,

		@field:NotEmpty(message = "JWT must be provided")
		@field:NotNull(message = "JWT must not be null")
		val jwt: String
)
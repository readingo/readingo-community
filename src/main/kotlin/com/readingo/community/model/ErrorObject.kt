package com.readingo.community.model

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity

sealed class Result

data class ErrorObject(val reason: String?, val success: Boolean = false) : Result()
data class SuccessObject(val success: Boolean = true) : Result()

fun makeNotFound(reason: String?): ResponseEntity<ErrorObject> {
	return makeStatus(reason, HttpStatus.NOT_FOUND)
}

fun makeConflict(reason: String?): ResponseEntity<ErrorObject> {
	return makeStatus(reason, HttpStatus.CONFLICT)
}

fun makeStatus(reason: String?, status: HttpStatus): ResponseEntity<ErrorObject> {
	return ResponseEntity(ErrorObject(reason), status)
}
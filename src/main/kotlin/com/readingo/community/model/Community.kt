package com.readingo.community.model

import org.hibernate.annotations.GenericGenerator
import javax.persistence.*
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

@Entity(name = "Community")
data class Community(
		@Id
		@GeneratedValue(generator = "system-uuid")
		@GenericGenerator(name = "system-uuid", strategy = "uuid")
		val id: String? = null,

		@field:NotEmpty(message = "Name must be provided")
		@field:NotNull(message = "name must not be null")
		val name: String,

		@field:NotEmpty(message = "Description must be provided")
		@field:NotNull(message = "Description must not be null")
		val description: String,

		@ElementCollection(targetClass = String::class, fetch = FetchType.LAZY)
		@JoinTable
		val books: MutableList<String>,

		@OneToMany(targetEntity = User::class, fetch = FetchType.LAZY)
		val members: MutableList<User>
) {
	fun addMember(user: User): Community {
		require(!this.members.contains(user)) { "User is already in the community" }
		this.members.add(user)
		return this
	}

	fun kickMember(user: User): Community {
		require(this.members.contains(user)) { "User is not in the community" }
		this.members.remove(user)
		return this
	}

	fun addBook(book: String): Community {
		require(!this.books.contains(book)) { "Book is already in the community" }
		this.books.add(book)
		return this
	}

	fun deleteBook(book: String): Community {
		require(this.books.contains(book)) { "Book is not in the community" }
		this.books.remove(book)
		return this
	}
}
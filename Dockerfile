FROM openjdk:8-jre-alpine
WORKDIR /app
EXPOSE 9002
ADD ./build/libs/community.jar .
ENTRYPOINT ["java", "-jar", "community.jar"]